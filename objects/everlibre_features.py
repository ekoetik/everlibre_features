#!/usr/bin/python
# -*- coding: utf-8 -*-
##############################################################################
#
#
#    Copyright (C) 2013 EVERLIBRE   Eric VERNICHON <evernichon@everlibre.ch>
#
#
#
##############################################################################
from osv import osv
from osv import fields
from osv.orm import Model, TransientModel, except_orm
import time
from tools.translate import _
import logging
import openerp

from openerp.addons.web.controllers.main import *


class everlibre_features_themes(Model):
    _name = 'everlibre.features.themes'
    _description = _('Themes')
    _columns = {
        'name': fields.char('Name', size=256),
        'stylesheet': fields.char('StyleSheet', size=512)
    }


everlibre_features_themes()


class everlibre_features_themes_selection(Model):
    _name = 'everlibre.features.themes.selection'
    _description = _('Themes')
    _columns = {
        'name': fields.many2one('everlibre.features.themes', 'Name'),
        'user_id': fields.many2one('res.users', "User"),

    }

    _defaults = {
        'user_id': lambda obj, cr, uid, context: uid,
    }


everlibre_features_themes_selection()
#
# class Menu(openerp.addons.web.controllers.main.Menu):
#     _cp_path = "/web/menu"
#
#     @openerpweb.jsonrequest
#     def load(self, req):
#         res = super(Menu, self).load(req)
#         print req.session.model('everlibre.features.themes.selection').search([('user_id','=',1)])
#
#
#         return res
