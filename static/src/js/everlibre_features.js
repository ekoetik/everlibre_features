openerp.everlibre_features = function (instance) {
    var QWeb = instance.web.qweb
    var _t = instance.web._t;
    var _lt = instance.web._lt;
    instance.web.form.SelectCreatePopup.include({


        setup_search_view: function(search_defaults) {
          var self = this;
        if (this.searchview) {
            this.searchview.destroy();
        }
        this.searchview = new instance.web.SearchView(this,
                this.dataset, false,  search_defaults);
        this.searchview.on('search_data', self, function(domains, contexts, groupbys) {
            if (self.initial_ids) {
                self.do_search(domains.concat([[["id", "in", self.initial_ids]], self.domain]),
                    contexts, groupbys);
                self.initial_ids = undefined;
            } else {
                self.do_search(domains.concat([self.domain]), contexts.concat(self.context), groupbys);
            }
        });
        this.searchview.on("search_view_loaded", self, function() {
            self.view_list = new instance.web.form.SelectCreateListView(self,
                    self.dataset, false,
                    _.extend({'deletable': false,
                        'selectable': !self.options.disable_multiple_selection,
                        'import_enabled': false,
                        '$buttons': self.$buttonpane,
                        'disable_editable_mode': true,
                        '$pager': self.$('.oe_popup_list_pager'),
                    }, self.options.list_view_options || {}));
            self.view_list.on('edit:before', self, function (e) {
                e.cancel = true;
            });
            self.view_list.popup = self;
            self.view_list.appendTo($(".oe_popup_list", self.$el)).then(function() {
                self.view_list.do_show();
            }).then(function() {
                self.searchview.do_search();
            });
            self.view_list.on("list_view_loaded", self, function() {
                self.$buttonpane.html(QWeb.render("SelectCreatePopup.search.buttons", {widget:self}));
                var $cbutton = self.$buttonpane.find(".oe_selectcreatepopup-search-close");
                $cbutton.click(function() {
                    self.destroy();
                });
                var $sbutton = self.$buttonpane.find(".oe_selectcreatepopup-search-select");
                $sbutton.click(function() {
                    self.select_elements(self.selected_ids);
                    self.destroy();
                });
                var $cbutton = self.$buttonpane.find(".oe_selectcreatepopup-search-create");
                if (self.context.eval()['create'] && self.context.eval()['create'] == 'inactive')
                {

                $cbutton.css('visibility','hidden');
                }
                $cbutton.click(function() {
                    self.new_object();
                });
            });
        });
        this.searchview.appendTo($(".oe_popup_search", self.$el));
    }

    });
    instance.web.form.FieldOne2ManyEasy = instance.web.form.FieldOne2Many.extend({
        init: function (field_manager, node) {
            this._super(field_manager, node);
        },
        start: function () {
            this._super.apply(this, arguments);
            this.$el.addClass('one2manyEasy');
            this.$el.removeClass('oe_list_editable');
            this.$el.contentEditable = false;
        }
    });
instance.web.ListView.include({
        load_list: function () {
            var self = this;
            this._super.apply(this, arguments);
            var links = document.getElementsByClassName("oe_list_button_import_excel");
            var links_pdf = document.getElementsByClassName("oe_list_button_import_pdf");
			for (index = 0; index < links.length; ++index)
				{
                links[index].onclick = function() {
                    self.export_to_excel("excel")
                };
			};
			for (index_pdf = 0; index_pdf < links_pdf.length; ++index_pdf)
				{
                links_pdf[index_pdf].onclick = function() {
                    self.export_to_excel("pdf")
                };
			};

        },
        export_to_excel: function(export_type) {
            var self = this
            var export_type = export_type
            view = this.getParent()
            // Find Header Element
            header_eles = self.$el.find('.oe_list_header_columns')
            header_name_list = []
            $.each(header_eles,function(){
                $header_ele = $(this)
                header_td_elements = $header_ele.find('th')
                $.each(header_td_elements,function(){
                    $header_td = $(this)
                    text = $header_td.text().trim() || ""
                    data_id = $header_td.attr('data-id')
                    if (text && !data_id){
                        data_id = 'group_name'
                    }
                    header_name_list.push({'header_name': text.trim(), 'header_data_id': data_id})
                   // }
                });
            });

            //Find Data Element
            data_eles = self.$el.find('.oe_list_content > tbody > tr')
            export_data = []
            $.each(data_eles,function(){
                data = []
                $data_ele = $(this)
                is_analysis = false
                if ($data_ele.text().trim()){
                //Find group name
	                group_th_eles = $data_ele.find('th')
	                $.each(group_th_eles,function(){
	                    $group_th_ele = $(this)
	                    text = $group_th_ele.text()
	                    is_analysis = true
	                    data.push({'data': text, 'bold': true})
	                });
	                data_td_eles = $data_ele.find('td')
	                $.each(data_td_eles,function(){
	                    $data_td_ele = $(this)
	                    text = $data_td_ele.text().trim() || ""
	                    if ($data_td_ele && $data_td_ele[0].classList.contains('oe_number') && !$data_td_ele[0].classList.contains('oe_list_field_float_time')){
	                        text = text.replace('%', '')
	                        text = instance.web.parse_value(text, { type:"float" })
	                        data.push({'data': text || "", 'number': true})
	                    }
	                    else{
	                        data.push({'data': text})
	                    }
	                });
	                export_data.push(data)
                }
            });

            //Find Footer Element

            footer_eles = self.$el.find('.oe_list_content > tfoot> tr')
            $.each(footer_eles,function(){
                data = []
                $footer_ele = $(this)
                footer_td_eles = $footer_ele.find('td')
                $.each(footer_td_eles,function(){
                    $footer_td_ele = $(this)
                    text = $footer_td_ele.text().trim() || ""
                    if ($footer_td_ele && $footer_td_ele[0].classList.contains('oe_number')){
                        text = instance.web.parse_value(text, { type:"float" })
                        data.push({'data': text || "", 'bold': true, 'number': true})
                    }
                    else{
                        data.push({'data': text, 'bold': true})
                    }
                });
                export_data.push(data)
            });

            //Export to excel
            $.blockUI();
            if (export_type === 'excel'){
                 view.session.get_file({
                     url: '/web/export/zb_excel_export',
                     data: {data: JSON.stringify({
                            model : view.model,
                            headers : header_name_list,
                            rows : export_data,
                     })},
                     complete: $.unblockUI
                 });
             }
             else{
                console.log(view)
                new instance.web.Model("res.users").get_func("read")(this.session.uid, ["company_id"]).then(function(res) {
                    new instance.web.Model("res.company").get_func("read")(res['company_id'][0], ["name"]).then(function(result) {
                        view.session.get_file({
                             url: '/web/export/zb_pdf_export',
                             data: {data: JSON.stringify({
                                    uid: view.session.uid,
                                    model : view.model,
                                    headers : header_name_list,
                                    rows : export_data,
                                    company_name: result['name']
                             })},
                             complete: $.unblockUI
                         });
                    });
                });
             }
        },
    });
instance.web.form.Many2ManyListView  = instance.web.form.Many2ManyListView.extend({
	do_activate_record: function(index, id) {
			var self = this;
			if (self.$("td[data-field='open_line']").length >0 ) {
				self.do_button_action('open_line',id,function (id) {
						$target.removeAttr('disabled');
						return self.reload_record(self.records.get(id)
					);
				});
			} else if  (self.$("td[data-field='open_line_invoice']").length >0 ) {
                self.do_button_action('open_line_invoice',id,function (id) {
                        $target.removeAttr('disabled');
                        return self.reload_record(self.records.get(id)
                    );
                });
            }
			else {
				this._super(index,id);
			}
		}});
instance.web.form.One2ManyListView = instance.web.form.One2ManyListView.extend({
		_template: 'One2Many.listview',
		init: function(parent, dataset, view_id, options) {
			var self = this;
			this._super(parent, dataset, view_id, options	);
		},
		do_activate_record: function(index, id) {
			var self = this;
			if (self.$("td[data-field='open_line']").length >0 ) {
				self.do_button_action('open_line',id,function (id) {
						$target.removeAttr('disabled');
						return self.reload_record(self.records.get(id)
					);
				});
			} else if  (self.$("td[data-field='open_line_invoice']").length >0 ) {
                self.do_button_action('open_line_invoice',id,function (id) {
                        $target.removeAttr('disabled');
                        return self.reload_record(self.records.get(id)
                    );
                });
            }
			else {
				this._super(index,id);
			}
		}
});

instance.web.form.FieldMany2ManyKanban = instance.web.form.FieldMany2ManyKanban.extend({
    init: function(field_manager, node) {
        var self = this;
        this._super(field_manager, node );
    },
    open_popup: function(type, unused) {
        if (self.$("button[data-name='open_line']").length >0 )
        {
            self.$("button[data-name='open_line']").click();

            }
            else if  (self.$("td[data-field='open_line_invoice']").length >0 ) {
                self.do_button_action('open_line_invoice',id,function (id) {
                        $target.removeAttr('disabled');
                        return self.reload_record(self.records.get(id)
                    );
                });
            }
        else
            {
        this._super(type, unused);
            }
    }

});

    instance.web.WebClient.include({
        apply_theme: function () {
            new instance.web.Model("everlibre.features.themes.selection").call('search', [
                    [
                        ['user_id', '=', this.session.uid]
                    ]
                ]).then(function (rec_ids) {
                    if (rec_ids.length > 0) {
                        new instance.web.Model("everlibre.features.themes.selection").call('read', rec_ids).then(function (res) {
                            if (res) {
                                new instance.web.Model("everlibre.features.themes").call('read', [res['name'][0]]).then(function (res) {
                                    if (res['stylesheet']) {
                                        $('head').append($('<link>', {
                                            'rel': 'stylesheet',
                                            'type': 'text/css',
                                            'href': res['stylesheet']
                                        }));
                                    }
                                });
                            }
                        });
                    }
                });
        },
        show_application: function () {
            this._super();

            this.apply_theme();
            jQuery.extend(
                jQuery.expr[ ":" ],
                { reallyvisible: function (a) {
                    return !(jQuery(a).is(':hidden') || jQuery(a).parents(':hidden').length);
                }}
            );

        },

        set_title: function (title) {
            self = this;
            title = _.str.clean(title);
            var sep = _.isEmpty(title) ? '' : ' - ';
            document.title = title + sep + this.session.db + sep + 'OpenERP';
            $('.ui-resizable').resizable();
            if (  $('.dbname').length ==0)
            {
                if (this && this.session && this.session.db)
                {
                 $('.oe_topbar').append('<span class="dbname" ><b class="dbname">'+  this.session.db.toUpperCase().replace('V70','') + '</b></span>'); 
                }
            }

            if (  $('#display_button').length ==0)
            {
            $('.oe_view_manager').prepend('<button class="display_button_close" id="display_button"  />');
            
            $('#display_button').click(function()
            {
                    $('#display_button').toggleClass('display_button_close');
                    $('#display_button').toggleClass('display_button_open');

                $('.oe_leftbar').toggleClass('leftmenu_display_none');
            }
                      );
                 }

        },
    });

    instance.web.UserMenu.include({
        do_update: function () {
            var self = this;
            this._super.apply(this, arguments);
        },
        on_menu_themes: function () {
            var self = this;

            if (!this.getParent().has_uncommitted_changes()) {
                self.rpc("/web/action/load", {
                    action_id: "everlibre_features.action_everlibre_features_themes_selection"
                }).done(function (result) {
                        result.res_id = instance.session.uid;
                        self.getParent().action_manager.do_action(result);
                    });
            }

        }

    });
    instance.web.form.widgets = instance.web.form.widgets.extend({
        'one2many_easy': 'instance.web.form.FieldOne2ManyEasy'
    });
};
