# -*- encoding: utf-8 -*-
##############################################################################
#
#    Copyright (c) 2011 EVERLIBRE. (http://www.everlibre.ch) All Rights Reserved.
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
{
    "name": "features module",
    "version": "1.0",
    "author": "EVERLIBRE",
    "category": "Localisation/Europe",
    "website": "http://www.everlibre.ch",
    "description": "Uniform module",
    "depends": ["base", "web"],
    'data': ['view/everlibre_features.xml', 'security/ir.model.access.csv'],
    'css': [
        'static/src/css/everlibre_features.css',
    ],
    'js': [
        'static/src/js/everlibre_features.js',
    ],
    'qweb': [
        'static/src/xml/everlibre_features.xml',
        'static/src/xml/web_printscreen_export.xml'

    ],
    'installable': True,
    'active': False,
    'certificate': '',
}
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
